from datetime import datetime

_TIMESTAMP_FORMAT = '%Y-%m-%dT%H:%M:%S.%fZ'


class MergeRequest:
    def __init__(self, title, created_at, updated_at, authors, reviewers, gitlab_url, reference_tag, approved_by):
        self.title = title
        self.created_at = datetime.strptime(created_at, _TIMESTAMP_FORMAT)
        self.updated_at = datetime.strptime(updated_at, _TIMESTAMP_FORMAT)
        self.authors = authors
        self.reviewers = reviewers
        self.gitlab_url = gitlab_url
        self.reference_tag = reference_tag
        self.approved_by = approved_by
