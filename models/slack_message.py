import re
from datetime import datetime
from typing import List

import slack_sdk.models.blocks.blocks as slack_blocks
import slack_sdk.models.messages.message as slack

from models.merge_request import MergeRequest

_TITLE_REGEX = re.compile(r"\[.*]\s*")
_SECTION_LIMIT = 10
_DAYS_VERBATIM = {
    0: 'today',
    1: 'yesterday'
}


class SlackMessage(slack.Message):
    def __init__(self, text: str = '', merge_requests: List[MergeRequest] = None, project_name: str = ''):
        super().__init__(text=text)
        self.project_name = project_name
        self.merge_requests = merge_requests or []

    def build(self) -> dict:
        if len(self.merge_requests) > 0:
            self.blocks.append(slack_blocks.HeaderBlock(text=":alert: {0} Open MR(s)".format(self.project_name)))
            self.blocks.append(slack_blocks.DividerBlock())
            self.blocks.extend(self._build_merge_request_blocks())
        return self.to_dict()

    def _build_merge_request_blocks(self):
        fields = []
        divider_text = slack_blocks.PlainTextObject(text="--------------------------------------")
        for index in range(len(self.merge_requests)):
            if index % 2 == 0 and index > 0:
                fields.append(divider_text)
                fields.append(divider_text)
            merge_request = self.merge_requests[index]
            fields.append(self._format_merge_request(merge_request))
        sections = []
        for idx in range(0, len(fields), _SECTION_LIMIT):
            sections.append(slack_blocks.SectionBlock(fields=fields[idx:idx + _SECTION_LIMIT]))
        return sections

    def _format_merge_request(self, merge_request):
        merge_request_content = filter(lambda content: content is not None, [
            self._format_merge_request_title(merge_request),
            self._format_authors(merge_request.authors),
            self._format_reviewers(merge_request.reviewers),
            self._format_approved_by(merge_request.approved_by),
            self._format_cycle_time(merge_request.created_at),
            self._format_activity(merge_request),
        ])
        return slack_blocks.MarkdownTextObject(text="\n".join(merge_request_content))

    @staticmethod
    def _format_activity(merge_request):
        unattended_since = (datetime.now() - merge_request.updated_at).days
        if unattended_since >= 2:
            return ':zombie: No activity since {0} days'.format(unattended_since)
        created_since = (datetime.now() - merge_request.created_at).days
        if created_since >= 2:
            return ':working: Updated {0}'.format(_DAYS_VERBATIM[unattended_since])
        return None

    @staticmethod
    def _format_cycle_time(created_at):
        pending_since = (datetime.now() - created_at).days
        if pending_since >= 2:
            return ':snail: Pending from {0} days'.format(pending_since)
        return ':clock10: Created {0}'.format(_DAYS_VERBATIM[pending_since])

    @staticmethod
    def _format_merge_request_title(merge_request):
        return ':gitlab-logo: *<{0}|MR {1}>* {2}'.format(merge_request.gitlab_url, merge_request.reference_tag,
                                                         _TITLE_REGEX.sub("", merge_request.title))

    @staticmethod
    def _format_authors(authors):
        return ':writing_hand: Authors: *%s*' % '*, *'.join(authors)

    @staticmethod
    def _format_reviewers(reviewers):
        return ':under-review: Reviewers: *%s*' % ('*, *'.join(reviewers) or 'NONE')

    @staticmethod
    def _format_approved_by(approved_by):
        if len(approved_by) > 0:
            return ':white_check_mark: Approved by: %s' % ', '.join(approved_by)
        return None
