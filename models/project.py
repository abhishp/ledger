class Project:
    def __init__(self, gitlab_id: int, name: str, slack_webhook: str, pr_labels: str):
        self.gitlab_id = gitlab_id
        self.name = name
        self.slack_webhook = slack_webhook
        self.pr_labels = pr_labels.split(',') if pr_labels else []
