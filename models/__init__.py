from .merge_request import MergeRequest
from .project import Project
from .slack_message import SlackMessage
