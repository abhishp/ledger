import json
import os
from typing import List

from dotenv import load_dotenv

from logger import Logger
from models import Project


def _get_project_config(name: str):
    project_config_name = '{0}_PROJECT_CONF'.format(name.upper())
    project_config = os.getenv(project_config_name, '{}')
    project = json.loads(project_config)
    try:
        return Project(project['gitlab_id'], project['name'], project['slack_webhook'], project.get('pr_labels'))
    except (KeyError, json.JSONDecodeError):
        Logger.error('Invalid config for %s project' % name)
        return None


class Config:
    def __init__(self):
        load_dotenv()
        project_names = os.getenv("PROJECTS_ENABLED", '').split(",")
        self._projects = list(map(_get_project_config, project_names))

    @staticmethod
    def retry_count():
        return int(os.getenv("RETRY_COUNT"))

    @staticmethod
    def gitlab_host():
        return os.getenv("GITLAB_HOST")

    @staticmethod
    def gitlab_token():
        return os.getenv("GITLAB_TOKEN")

    @property
    def projects(self) -> List[Project]:
        return self._projects
