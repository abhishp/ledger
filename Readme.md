# Ledger

Ledger is a tool to publish daily `PR Reports` of different projects from Gitlab to Slack. It has the following
functionalities:

- It retrieves open MRs from Gitlab and filters out Draft/WIP MRs.
- It fetches the Authors/Reviewers/Created Time/Link of the MR.
- It publishes this data on any Slack channel as per the requirement.

Currently, running via Gitlab Scheduler via `ledger_run` job.

### Project Setup (Alternate)

- Python3 ([How to setup Python3?](https://docs.python-guide.org/starting/install3/linux/))
- Create [Slack App](https://api.slack.com/apps) and register a Webhook url.
- Add the project name in the `PROJECTS_ENABLED` environment variable as a CSV.
- Add Project's Gitlab id, title and slack webhook in `[PROJECT_NAME]_PROJECT_CONF` environment variable.
- Copy `setup.sh` from this repo to your home directory and then run it.

Can be placed on any Integration box. Execution of this code can be managed via shell script `ledger.sh`
running on `crontab`

```shell
$ crontab -l
> 30 11 * * 1-5 /bin/bash /etc/ledger/ledger.sh
```

### Testing
- Please join the  #novelty-ledger-test channel on slack.

#### On push
- Pushing the code will publish the MR status for ledger by default to the above mentioned channel.
	- This can be modified by changing the pipeline variables for your Merge request on gitlab.

#### Local
- Copy .env.sample and setup the configs for the channel you desire. 
  (The webhook for #novelty-ledger-test can be found in gitlab pipeline environment variables)
- Run `python3 -u main.py` and this would process and publish the MR stats for the configured projects.
