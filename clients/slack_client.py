import time

import requests


class SlackClient:
    def __init__(self, retry_count: int):
        self.retry_count = retry_count

    def send_with_retry(self, message: dict, webhook_url: str) -> None:
        sleep_time = 0.1
        for count in range(self.retry_count):
            status_code = self._send_to_slack(message, webhook_url)
            if status_code == 200:
                break
            time.sleep(sleep_time)
            sleep_time = sleep_time * 2

    @staticmethod
    def _send_to_slack(message: dict, webhook_url: str) -> int:
        return requests.post(webhook_url, json=message).status_code
