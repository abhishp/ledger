from typing import List

from gitlab import Gitlab
from gitlab.v4.objects import MergeRequest


class GitlabClient:
    def __init__(self, host: str, private_token: str):
        self.gitlab = Gitlab(host, private_token=private_token)
        self.gitlab.auth()

    def get_open_mr_for_project(self, project_id: int, pr_labels: List[str]) -> List[MergeRequest]:
        project = self.gitlab.projects.get(project_id)
        return project.mergerequests.list(state='opened', order_by='updated_at', wip='no', labels=pr_labels)
