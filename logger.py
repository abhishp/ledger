import logging

logging.Formatter.default_msec_format = '%s.%03d'
logging.basicConfig(level=logging.INFO, format='[%(asctime)s][%(name)s] %(message)s')

Logger = logging.getLogger('Ledger')
