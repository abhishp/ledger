from clients import GitlabClient, SlackClient
from config import Config
from ledger import Ledger
from logger import Logger
from services import GitlabService

if __name__ == "__main__":
    try:
        Logger.info('Initializing config and services...')
        config = Config()
        gitlab_service = GitlabService(GitlabClient(config.gitlab_host(), config.gitlab_token()))
        slack_client = SlackClient(config.retry_count())

        Ledger(gitlab_service, slack_client).audit(config.projects)
    except Exception as ex:
        Logger.error("Unexpected error: ", exc_info=ex)
        quit(1)
