#!/bin/bash

#To avoid any locale error
export LC_ALL="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"

echo "cloning project"
git clone https://source.golabs.io/go-food/ledger.git
echo "cloning successful"

sudo apt-get install python3-venv
sudo apt-get install python3-pip
cd ledger

echo "upgrade PIP"
python3 -m pip install --upgrade --force pip

echo "install venv"
pip3 install virtualenv

echo "Create VENV"
python3 -m venv venv

echo "Setup crontab "
crontab -l | { cat; echo "30 11 * * 1-5 /bin/bash ~/ledger/ledger.sh"; } | crontab -
echo "Done: Printing latest crontab"
crontab -l