#!/bin/bash

sudo service cron restart
cd ~/ledger
. venv/bin/activate
pip install -r requirements.txt
python3 main.py
deactivate
