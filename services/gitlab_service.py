import re
from typing import List

from clients import GitlabClient
from models import MergeRequest

_USER_REGEX = re.compile(r"@\w*\.\w*")
_DANGER_REVIEWERS_NOTE = 'will be reviewing this MR. The first review will be completed within 2 working days'


def _build_merge_request(mr) -> MergeRequest:
    return MergeRequest(mr.title, mr.created_at, mr.updated_at, _get_authors(mr), _get_reviewers(mr),
                        mr.web_url, mr.reference, _get_approved_by(mr))


def _get_authors(mr):
    authors = set(_USER_REGEX.findall(mr.description))
    authors.add('@%s' % mr.author['username'])
    return list(authors)


def _get_reviewers(mr):
    reviewers = set(map(lambda assignee: '@%s' % assignee['username'], mr.assignees + mr.reviewers))
    for note in mr.notes.list(per_page=1000):
        body = note.attributes['body']
        if body.find(_DANGER_REVIEWERS_NOTE) != -1:
            reviewers.update(_USER_REGEX.findall(body))
    return list(reviewers)


def _get_approved_by(mr):
    return list(map(lambda a: '@%s' % a['user']['username'], mr.approvals.get().approved_by))


class GitlabService:
    def __init__(self, gitlab_client: GitlabClient):
        self.gitlab_client = gitlab_client

    def get_open_merge_requests(self, project_id: int, pr_labels: List[str]) -> object:
        merge_requests = self.gitlab_client.get_open_mr_for_project(project_id, pr_labels)
        mrs = list(map(_build_merge_request, merge_requests))
        mrs.sort(key=lambda mr: mr.created_at)
        return mrs
