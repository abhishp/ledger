from collections import defaultdict
from typing import List, Dict

from clients import SlackClient
from logger import Logger
from models import SlackMessage, Project
from services import GitlabService


class Ledger:
    def __init__(self, gitlab_service: GitlabService, slack_client: SlackClient):
        self.slack_client = slack_client
        self.gitlab_service = gitlab_service

    def audit(self, projects: List[Project]) -> None:
        Logger.info('Auditing %d project(s)' % len(projects))
        mr_stats_per_channel = defaultdict(lambda: 0)
        for project in projects:
            mr_stats_per_channel[project.slack_webhook] += self._audit_project(project)
        self._notify_inactivity(mr_stats_per_channel)
        Logger.info('All project(s) audited successfully')

    def _notify_inactivity(self, mr_stats_per_channel: Dict[str, int]):
        for slack_webhook, mr_count in mr_stats_per_channel.items():
            if mr_count == 0:
                msg = ':party_dinosour: *Hurray!! No pending merge requests to be reviewed* :confetti_ball:'
                self.slack_client.send_with_retry(SlackMessage(text=msg).build(), slack_webhook)

    def _audit_project(self, project: Project) -> int:
        Logger.info('Fetching open merge requests for %s' % project.name)
        merge_requests = self.gitlab_service.get_open_merge_requests(project.gitlab_id, project.pr_labels)
        if len(merge_requests) > 0:
            message = SlackMessage(merge_requests=merge_requests, project_name=project.name).build()
            Logger.info('Publishing report to slack for %d merge request(s)' % len(merge_requests))
            self.slack_client.send_with_retry(message, project.slack_webhook)
        else:
            Logger.info('No open MR found')
        return len(merge_requests)
