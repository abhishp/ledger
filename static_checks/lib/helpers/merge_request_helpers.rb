module MergeRequestHelpers
  def project_id
    merge_request.project_id
  end

  def merge_request_id
    merge_request.iid
  end

  def merge_request
    @merge_request ||= env.request_source.mr_json
  end

  def draft?
    merge_request.title.start_with? Constants::WIP_PREFIX, Constants::DRAFT_PREFIX
  end

  def authors
    merge_request.description.to_s.scan(Constants::AUTHORS_REGEX)
  end
end
