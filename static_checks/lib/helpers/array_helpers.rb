module ArrayHelpers
  def format_list(list)
    return '' if list.empty?
    return list[0].to_s if list.length == 1

    "#{list[0..-2].join(', ')} and #{list[-1]}"
  end
end
