module Checks
  autoload :File, 'checks/file'
  autoload :MergeRequest, 'checks/merge_request'

  class Check
    include DangerProxy
    include MergeRequestHelpers

    def verify!
      raise('implement in derived class')
    end
  end

  def self.register(check)
    raise ArgumentError.new("cannot register #{check.inspect} as static check. Must be a subclass of Checks::Check") unless check <= Check
    @checks ||= []
    @checks.push(check)
  end

  def self.perform!
    @checks.each do |check|
      check.new.verify!
    end
  end

  register File
  register MergeRequest
end