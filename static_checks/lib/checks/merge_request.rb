module Checks
  class MergeRequest < Check
    def verify!
      [:destination_branch, :mention_authors, :suitable_description].each do |check|
        self.send(check) if Configuration.mr_checks_enabled? check
      end
    end

    private

    def mention_authors
      failure('Authors must be mentioned in the description e.g. @(username)') if authors.empty?
    end

    def suitable_description
      warn("Please provide a suitable description. There's also a [MR template](#{Configuration.description_template_path}) available to use.") unless gitlab.mr_body.include?('### Description')
    end

    def destination_branch
      warn('Please set the destination branch to master') unless gitlab.branch_for_base == 'master'
    end
  end
end