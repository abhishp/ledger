module Checks
  class File < Check

    def initialize
      @errors = { swagger:  'Contract changes detected, please update Swagger',
                  cassette: 'Cassette changes detected, please verify and commit any changes to cassettes' }
    end

    def verify!
      [:swagger, :cassette].each do |check|
        verify_empty_diff_file(check) if Configuration.file_checks_enabled? check
      end
    end

    private

    def verify_empty_diff_file(type)
      filename = ::File.join(ENV["CI_PROJECT_DIR"],"#{type}.diff")
      return unless ::File.exist?(filename)

      failure(@errors[type]) unless ::File.zero?(filename)
    end

  end
end
