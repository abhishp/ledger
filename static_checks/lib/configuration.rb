class Configuration
  class << self
    def users
      @users ||= get_multi_value_config('DANGER_PROJECT_USERS')
    end

    def reviewer_count
      @reviewer_count ||= ENV.fetch('DANGER_REVIEWERS_COUNT', 2).to_i
    end

    def project_name
      @project_name ||= ENV.fetch('DANGER_PROJECT_NAME', 'danger')
    end

    def file_checks_enabled?(check)
      check_enabled?(:file, check)
    end

    def mr_checks_enabled?(check)
      check_enabled?(:mr, check)
    end

    def description_template_path
      ENV.fetch('DANGER_DESCRIPTION_TEMPLATE_PATH', 'https://source.golabs.io/go-food/ledger/-/blob/master/.gitlab/merge_request_templates/Story.md')
    end

    private

    def get_multi_value_config(name, defaults = '')
      ENV.fetch(name, defaults).split(',').map(&:strip)
    end

    def setup_checks_config(type)
      @checks ||= {}
      return unless @checks[type].nil?

      config        = get_multi_value_config("DANGER_ENABLED_#{type.upcase}_CHECKS", default_checks[type])
      @checks[type] = config.map { |c| c.downcase.to_sym }.to_set
    end

    def check_enabled?(type, check)
      setup_checks_config(type)
      @checks[type].include? check
    end

    def default_checks
      { file: 'swagger,cassette', mr: 'destination_branch,mention_authors,suitable_description' }
    end
  end
end
