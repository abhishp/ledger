module DangerProxy
  def self.set_instance(danger_file)
    $_danger_proxy_instance_ = danger_file
  end

  def method_missing(method_sym, *arguments, **keyword_arguments, &block)
    return super if $_danger_proxy_instance_.nil?
    if keyword_arguments.empty?
      $_danger_proxy_instance_.send(method_sym, *arguments, &block)
    else
      $_danger_proxy_instance_.send(method_sym, *arguments, **keyword_arguments, &block)
    end
  end
end
