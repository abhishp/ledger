class ReviewerRoulette
  include MergeRequestHelpers
  include DangerProxy

  def fetch_reviewers
    reviewers = existing_reviewers
    return format_reviewers(reviewers) if reviewers.length >= Configuration.reviewer_count

    format_reviewers(random_reviewers)
  end

  private

  def format_reviewers(reviewers)
    reviewers.map { |reviewer| "@#{reviewer}" }
  end

  def assignees_from_comments
    comment = env.request_source.mr_comments.find do |comment|
      comment.generated_by_danger? Configuration.project_name
    end
    return [] if comment.nil?

    reviewers = comment.body.scan(Constants::AUTHORS_REGEX)
    return [] if reviewers.empty?

    reviewers.map { |assignee| assignee.gsub('@', '') }
  end

  def existing_reviewers
    @existing_reviewers ||= assignees_from_comments.union(merge_request.assignees.map(&:username))
  end

  def random_reviewers
    all_reviewers = Configuration.users
    (authors + existing_reviewers).each { |author| all_reviewers.delete(author.gsub('@', '')) }
    existing_reviewers + all_reviewers.shuffle.sample(Configuration.reviewer_count - existing_reviewers.length)
  end
end
