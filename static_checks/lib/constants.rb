module Constants
  DRAFT_PREFIX = 'Draft: '.freeze
  WIP_PREFIX = 'WIP'.freeze
  REVIEWERS_SENTENCE = 'will be reviewing this MR. The first review will be completed within 2 working days.'.freeze
  AUTHORS_REGEX = /@\w+\.?\w+/.freeze
end
