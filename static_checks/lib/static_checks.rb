$LOAD_PATH.unshift File.dirname(__FILE__)

autoload :Configuration, 'configuration'
autoload :Constants, 'constants'
autoload :DangerProxy, 'danger_proxy'
autoload :Checks, 'checks/checks'
autoload :ArrayHelpers, 'helpers/array_helpers'
autoload :MergeRequestHelpers, 'helpers/merge_request_helpers'
autoload :ReviewerRoulette, 'models/reviewer_roulette'

class StaticChecks
  include ArrayHelpers
  include DangerProxy
  include MergeRequestHelpers

  def perform!
    Checks.perform!
    failed? ? mark_as_draft : assign_reviewers
  end

  private

  def assign_reviewers
    return if draft? || authors.empty?
    message("#{format_list(ReviewerRoulette.new.fetch_reviewers)} #{Constants::REVIEWERS_SENTENCE}")
  end

  def mark_as_draft
    return if draft?
    gitlab.api.update_merge_request(project_id, merge_request_id, { title: Constants::DRAFT_PREFIX + gitlab.mr_title })
  end
end
