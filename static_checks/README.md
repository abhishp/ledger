Static Checks
---

This folder contains a Dangerfile that can be used for certain static checks on a merge request:

1. Check if the authors have been mentioned in the description
2. Check if the merge request has a proper description
3. Check if the destination branch is master
4. Check if there was any diff generated in the Swagger API spec or cassettes (based on files added during the test job)

Adding a new check is simple, just create a new file under `lib/checks`, inherit the `Check` class and implement
the `verify!` method, now register the check in `checks.rb`

Following configuration can also be utilised:

|Description|Key|Type|Default| 
|:----:|:-----------:|:-------------:|:---:|
|Project Name|`DANGER_PROJECT_NAME`| string | danger |
|Number of reviewers to be assigned|`DANGER_REVIEWERS_COUNT`| integer | 2 |
|Users for the project|`DANGER_PROJECT_USERS`| csv ||
|Enable file checks|`DANGER_ENABLED_FILE_CHECKS`| csv | swagger,cassette |
|Enable MR checks|`DANGER_ENABLED_MR_CHECKS`| csv | destination_branch,mention_authors,suitable_description |
|Link to an MR description template|`DANGER_DESCRIPTION_TEMPLATE_PATH`| string |https://source.golabs.io/go-food/ledger/-/blob/master/.gitlab/merge_request_templates/Story.md|

### How do I integrate this with my project?
The static_checks follow a simple plug and play model to allow easy integration with any project on GitLab.
All you need to do is add the following lines at the top of your `.gitlab-ci.yml`
```yaml
include:
  - project: 'go-food/ledger'
    file: '/static_checks/.static-checks-ci.yml'
```

By default, the static_checks job would run in `static_checks` stage which can be easily overridden along with any other 
configs mentioned above in either the Environment for the GitLab CI or just using the stage/job variables in the `.gitlab-ci.yml`

#### Overriding default `static_checks` template
If you want to run the static checks job in a different stage or as part of an existing job, just add an override to your `.gitlab-ci.yml` as following:
```yaml
static_checks:
  stage: <stage-name>
```

Similarly, you can also override the `before_script`, `script` and [other attributes](https://docs.gitlab.com/ee/ci/yaml/README.html#job-keywords) of the job.
