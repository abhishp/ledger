require_relative 'lib/static_checks'

DangerProxy.set_instance(binding.receiver)

StaticChecks.new.perform!
