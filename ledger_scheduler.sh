#!/bin/bash

pip3 install virtualenv
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
python3 main.py
deactivate